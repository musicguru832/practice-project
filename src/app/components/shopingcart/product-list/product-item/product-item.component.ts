import { MessangerService } from './../../../../services/messanger.service';
import { Product } from './../../../../models/product';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-product-item',
  templateUrl: './product-item.component.html',
  styleUrls: ['./product-item.component.sass']
})
export class ProductItemComponent implements OnInit {
@Input()productItem: Product
  constructor(private messangeService:MessangerService) { }
  ngOnInit(): void {
  }

  handelAddTocart(){
    this.messangeService.sendMsg(this.productItem)
  }

}
