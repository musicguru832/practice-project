import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/shared/header/header.component';
import { FooterComponent } from './components/shared/footer/footer.component';
import { NavComponent } from './components/shared/nav/nav.component';
import { ShopingcartComponent } from './components/shopingcart/shopingcart.component';
import { FiltersComponent } from './components/shopingcart/filters/filters.component';
import { ProductListComponent } from './components/shopingcart/product-list/product-list.component';
import { CartComponent } from './components/shopingcart/cart/cart.component';
import { CartItemComponent } from './components/shopingcart/cart-item/cart-item.component';
import { ProductItemComponent } from './components/shopingcart/product-list/product-item/product-item.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    NavComponent,
    ShopingcartComponent,
    FiltersComponent,
    ProductListComponent,
    CartComponent,
    CartItemComponent,
    ProductItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
