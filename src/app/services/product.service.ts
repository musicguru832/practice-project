import { productUrl } from './../../config/api';
import { Product } from './../models/product';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';





@Injectable({
  providedIn: 'root'
})
export class ProductService {
  // products:Product[]=[
  //   new Product(1,'product1','this is product 1 description',200),
  //   new Product(2,'product2','this is product 2 description',300),
  //   new Product(3,'product3','this is product 3 description',400),
  //   new Product(4,'product4','this is product 4 description',500),
  //   new Product(5,'product5','this is product 5 description',600),
  //   new Product(6,'product6','this is product 6 description',700),
  //   new Product(7,'product7','this is product 7 description',800),
  //   new Product(8,'product8','this is product 8 description',900),
  // ]
  constructor(private http:HttpClient) { }

  getProducts():Observable<Product[]>{
    return this.http.get<Product[]>(productUrl)
  }
}
